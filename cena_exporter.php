<?php
if (count($argv) !== 2) {
  print("用法: {$argv[0]} [Cena比赛的src目录]\n");
  exit(1);
}

$src_dir = $argv[1];
$results = array();
foreach (scandir($src_dir) as $dir) {
  if ($dir[0] != '.' and file_exists("$src_dir/$dir/result.xml")) {
    $results[$dir] = simplexml_load_file("$src_dir/$dir/result.xml");
  }
}

$id_delimiters = array('_', '-');
$verdict_repr = "  RTMYBAWP* []?^-";

foreach ($results as $id => $xml) {
  $score = 0;
  $verdicts = array();
  foreach ($xml->result->problem as $problem) {
    $verdict = "";
    foreach ($problem->testcase as $testcase) {
      $verdict .= $verdict_repr[$testcase["status"] * 1];
      $score += $testcase["score"] * 1;
    }
    $verdicts[] = $verdict;
  }

  $id_delimited = str_replace($id_delimiters, "\t", $id);
  print($id_delimited);
  foreach ($verdicts as $verdict) {
    print("\t$verdict");
  }
  print("\t$score\n");
}

print <<<REPR

R=无法运行
T=超时
M=超内存
Y=运行时错误
B=崩溃
A=正确
W=错误的答案
P=得部分分
*=程序无输出
[=缺标准输入
]=无标准输出
?=无程序
^=自定义评测错误
-=编译错误

REPR;
